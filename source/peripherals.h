/* --COPYRIGHT--,BSD
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
/*
 * ======== hal.c ========
 *
 */
#include <MSP430.h>
#include <stdint.h>
#include "descriptors.h"

#ifndef _PERIPHERALS_H_
#define _PERIPHERALS_H_

#ifdef __cplusplus
extern "C"
{
#endif

// Maximal host system delay
#define MAX_SYS_DELAY   20  // ms

// Size of Rx/Tx UART Buffers
// Value should be big enough to cover max system delay
#define RX_BUFFER_SIZE  1280             // max_baud * SYS_DELAY ms/1000/8bit
#define TX_BUFFER_SIZE  MAX_PACKET_SIZE  // Don't modyfy this value!!!


enum active_peripherals{None, UART, I2C, SPI};
extern enum active_peripherals active_peripheral;
extern uint8_t rxBuffer[RX_BUFFER_SIZE];
extern uint8_t txBuffer[TX_BUFFER_SIZE];
extern uint8_t *txBuffer_read_ptr;
extern uint8_t *txBuffer_write_ptr;

uint8_t BaudrateSelect(uint32_t lBaudrate);

extern uint32_t current_baudrate;

void disable_peripherals();

void startup_led_sequence();  // Blink the LEDs
#define LED_POUT  P1OUT
#define LED1_PIN  BIT0
#define LED2_PIN  BIT1
#define LED1_ON   {LED_POUT |= LED1_PIN;}
#define LED1_OFF  {LED_POUT &= ~LED1_PIN;}
#define LED1_TOGGLE  {LED_POUT ^= LED1_PIN;}
#define LED2_ON   {LED_POUT |= LED2_PIN;}
#define LED2_OFF  {LED_POUT &= ~LED2_PIN;}
#define LED2_TOGGLE  {LED_POUT ^= LED2_PIN;}

#define LED_BLINK_DELAY  5000000
#define LED_BLINK_COUNT  3

/*** BSL entry sequence ******************************************************/

#define ENTRY_SEQ_PDIR  PJDIR
#define ENTRY_SEQ_POUT  PJOUT
#define RESET_PIN   BIT1
#define TEST_PIN    BIT2
#define TCK_PIN     BIT3

#define RESET_HIGH  {ENTRY_SEQ_POUT |= RESET_PIN;}
#define RESET_LOW   {ENTRY_SEQ_POUT &= ~RESET_PIN;}
#define TEST_HIGH   {ENTRY_SEQ_POUT |= TEST_PIN;}
#define TEST_LOW    {ENTRY_SEQ_POUT &= ~TEST_PIN;}
#define TCK_HIGH    {ENTRY_SEQ_POUT |= TCK_PIN;}
#define TCK_LOW     {ENTRY_SEQ_POUT &= ~TCK_PIN;}

#define INVOKE_DELAY 10000

// Timing for Pin Toggling during BSL Entry Sequence
// TEST PIN reset time = 10us (must be less than 15us)
#define BSL_ENTRY_SEQUENCE_TIME  50

void BSL_invoke_sequence();  // Toggle RST/TST/TCK pins for BSL invoke
void BSL_reset_sequence();  // Reset sequence to leave BSL (JTAG20 workaround)


/*** UART ********************************************************************/
uint8_t InitUart(uint32_t lBaudrate);
void uartControl();

/*** I2C *********************************************************************/
#define BSL_SLAVE_ADDR  0x48

#define I2C_PORT_SEL  P4SEL
#define I2C_PORT_OUT  P4OUT
#define I2C_PORT_REN  P4REN
#define I2C_PORT_DIR  P4DIR

#define SDA_PIN  BIT5
#define SCL_PIN  BIT0
#define SCL_CLOCK_DIV(X)  (USB_MCLK_FREQ/X)

#define BSL_NO_RESPONSE_REQUIRED          0xAE
#define BSL_ERROR_HEADER_INCORRECT        0x51
#define BSL_ERROR_INCORRECT_RESPONSE_CRC  0xA0
#define BSL_ERROR_OK                      0x00

#define I2C_HEADER         0x80
#define I2C_HEADER_LENGTH  1     // length in byte
#define I2C_LENGTH_LENGTH  2     // length in byte
#define I2C_CRC_LENGTH     2     // length in byte

// Init I2C module B0 and according port settings
int8_t InitI2C(unsigned char eeprom_i2c_address, uint32_t bitrate);
int16_t i2cSendMessage(unsigned char* I2cMessage, int messageLength);
int16_t i2cReceiveMessage(unsigned char* I2cMessage);
void i2cBslEntrySequence(void);
void i2cStopSending(void);
unsigned int UART_FSM(unsigned char *dataBuffer);

void i2cControl(void);


/*** SPI *********************************************************************/
#define SPI_CRC_LENGTH  2  // length in byte

enum spiStates {idle, TXheader, TXlengthLow, TXlengthHigh, TXreceive, RXheader, RXlengthLow, RXlengthHigh, RXreceive};

// Future enhancement for SPI

uint8_t spiInit(uint32_t lBaudrate);
void spiControl();

#ifdef __cplusplus
}
#endif

#endif