/* --COPYRIGHT--,BSD
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
/*
 * ======== hal.c ========
 *
 */
#include "peripherals.h"
#include "USB_API/USB_CDC_API/UsbCdc.h"
#include "USB_app/usbConstructs.h"

enum active_peripherals active_peripheral = None;

uint8_t rxBuffer[RX_BUFFER_SIZE];
uint8_t txBuffer[TX_BUFFER_SIZE];
uint8_t *txBuffer_read_ptr = txBuffer;
uint8_t *txBuffer_write_ptr = txBuffer;
#define BUFFER_SIZE 266
uint8_t UARTtoCDC_CircularBuffer[BUFFER_SIZE];
uint16_t UARTtoCDC_RXPtr = 0;
uint16_t UARTtoCDC_TXPtr = 0;

uint32_t current_baudrate = 0;

/* Blink the LEDs */
void startup_led_sequence()
{
    uint8_t led_blink_count;
    for(led_blink_count = LED_BLINK_COUNT; led_blink_count > 0; --led_blink_count)
    {
    LED1_ON
    LED2_ON
    __delay_cycles(LED_BLINK_DELAY);
    LED1_OFF
    LED2_OFF
    __delay_cycles(LED_BLINK_DELAY);
    }
}


/*** BSL entry sequence ******************************************************/

/* BSL entry sequence: Toggle RST/TEST/TCK pins for BSL invoke */
void BSL_invoke_sequence()
{
    /* BSL invoke sequence
     *
     *       H --------+           +-----------
     * RST   L         +-----------+
     *
     *       H --------+  +--+  +-----+
     * TEST  L         +--+  +--+     +--------
     *
     *       H -----------+  +--+     +--------
     * TCK   L            +--+  +-----+
     */

    // Set RST, TST and TCK pin to output
    ENTRY_SEQ_PDIR |= (RESET_PIN + TEST_PIN + TCK_PIN);

    /* Start with RST, TEST and TCK high */
    ENTRY_SEQ_POUT = RESET_PIN + TEST_PIN + TCK_PIN;
    __delay_cycles(INVOKE_DELAY);
    __delay_cycles(INVOKE_DELAY);
    __delay_cycles(INVOKE_DELAY);

    /* RST low, TEST low, TCK high */
    ENTRY_SEQ_POUT = TCK_PIN;
    __delay_cycles(INVOKE_DELAY);

    /* RST low, TEST high, TCK low */
    ENTRY_SEQ_POUT = TEST_PIN;
    __delay_cycles(INVOKE_DELAY);

    /* RST low, TEST low, TCK high */
    ENTRY_SEQ_POUT = TCK_PIN;
    __delay_cycles(INVOKE_DELAY);

    /* RST low, TEST high, TCK low */
    ENTRY_SEQ_POUT = TEST_PIN;
    __delay_cycles(INVOKE_DELAY);

    /* RST high, TEST high, TCK low */
    ENTRY_SEQ_POUT = RESET_PIN + TEST_PIN;
    __delay_cycles(INVOKE_DELAY);

    /* RST high, TEST low, TCK high */
    ENTRY_SEQ_POUT = RESET_PIN + TCK_PIN;
    __delay_cycles(INVOKE_DELAY);
    __delay_cycles(INVOKE_DELAY);
    __delay_cycles(INVOKE_DELAY);

    // Set RST, TST and TCK pin to input
    ENTRY_SEQ_PDIR &= ~(RESET_PIN + TEST_PIN + TCK_PIN);
}


/* Reset sequence to leave BSL (JTAG20 workaround) */
void BSL_reset_sequence()
{
    /* BSL reset sequence (JTAG20 workaround)
     *
     *       H --------+              +---------
     * RST   L         +--------------+
     *
     *       H --------+  +--+  +--+
     * TEST  L         +--+  +--+  +------------
     */

    // Set RST, TST and TCK pin to output
    ENTRY_SEQ_PDIR |= (RESET_PIN + TEST_PIN + TCK_PIN);

    /* Start with RST and TEST high */
    ENTRY_SEQ_POUT = RESET_PIN + TEST_PIN;
    __delay_cycles(INVOKE_DELAY);
    __delay_cycles(INVOKE_DELAY);
    __delay_cycles(INVOKE_DELAY);

    /* RST low, TEST low */
    ENTRY_SEQ_POUT = 0;
    __delay_cycles(INVOKE_DELAY);

    /* RST low, TEST high */
    ENTRY_SEQ_POUT = TEST_PIN;
    __delay_cycles(INVOKE_DELAY);

    /* RST low, TEST low */
    ENTRY_SEQ_POUT = 0;
    __delay_cycles(INVOKE_DELAY);

    /* RST low, TEST high */
    ENTRY_SEQ_POUT = TEST_PIN;
    __delay_cycles(INVOKE_DELAY);

    /* RST low, TEST low */
    ENTRY_SEQ_POUT = 0;
    __delay_cycles(INVOKE_DELAY);

    /* RST high, TEST low */
    ENTRY_SEQ_POUT = RESET_PIN;
    __delay_cycles(INVOKE_DELAY);
    __delay_cycles(INVOKE_DELAY);
    __delay_cycles(INVOKE_DELAY);

    // Set RST, TST and TCK pin to input
    ENTRY_SEQ_PDIR &= ~(RESET_PIN + TEST_PIN + TCK_PIN);
}


/* BaudrateSelect
 * Handles configuration of UART, I2C and SPI peripheral on baud rate change */
uint8_t BaudrateSelect(uint32_t lBaudrate)
{
    if(current_baudrate == lBaudrate)
    {
        return 0;
    }

    current_baudrate = lBaudrate;

    // Reset peripheral communication pins to input
    P4DIR &= ~(BIT0 + BIT3 + BIT4 + BIT5);
    // Set RST, TST and TCK pin to input
    ENTRY_SEQ_PDIR &= ~(RESET_PIN + TEST_PIN + TCK_PIN);

    LED1_OFF
    __delay_cycles(1000000);
    LED1_ON

    uint8_t baudIndex;

    switch(lBaudrate)
    {
        /* UART peripheral */
        case 1200:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;
        case 2400:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;
        case 4800:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;
        case 4801:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;
        case 4802:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;
        case 9600:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;
        case 9601:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;
        case 19200:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;
        case 38400:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;
        case 57600:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;
        case 115200:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;
        case 230400:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;
        case 460800:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;
        case 921600:
            active_peripheral = UART;
            baudIndex = InitUart(lBaudrate);
            break;

        /* I2C peripheral */
        case 100000:
            active_peripheral = I2C;
            baudIndex = InitI2C(BSL_SLAVE_ADDR, lBaudrate);
            break;
        case 100001:
            active_peripheral = I2C;
            baudIndex = InitI2C(BSL_SLAVE_ADDR, lBaudrate);
            break;
        case 400000:
            active_peripheral = I2C;
            baudIndex = InitI2C(BSL_SLAVE_ADDR, lBaudrate);
            break;
        case 400001:
            active_peripheral = I2C;
            baudIndex = InitI2C(BSL_SLAVE_ADDR, lBaudrate);
            break;

        /* SPI */
        case 125000:
            active_peripheral = SPI;
            baudIndex = spiInit(lBaudrate);
            break;
        case 125001:
            active_peripheral = SPI;
            baudIndex = spiInit(lBaudrate);
            break;
        case 250000:
            active_peripheral = SPI;
            baudIndex = spiInit(lBaudrate);
            break;
        case 250001:
            active_peripheral = SPI;
            baudIndex = spiInit(lBaudrate);
            break;
        case 500000:
            active_peripheral = SPI;
            baudIndex = spiInit(lBaudrate);
            break;
        case 500001:
            active_peripheral = SPI;
            baudIndex = spiInit(lBaudrate);
            break;
        case 1000000:
            active_peripheral = SPI;
            baudIndex = spiInit(lBaudrate);
            break;
        case 1000001:
            active_peripheral = SPI;
            baudIndex = spiInit(lBaudrate);
            break;

        default:
            baudIndex = 0;
            break;

    }

    return baudIndex;
}


/*** UART ********************************************************************/
// List of standard baudrates
const struct _BaudrateList{
    uint8_t ucaBR0, ucaBR1, ucaMCTL;
}BaudrateList[] = {                                   // CLK 20MHz
        {0x00,    0x00,   0x00},
        {0x1A,    0x41,   UCBRS_5},                   // 1.2 kb/s
        {0x8D,    0x20,   UCBRS_3},                   // 2.4 kb/s
        {0x46,    0x10,   UCBRS_5},                   // 4.8 kb/s
        {0x82,    0x00,   0x30 + UCOS16},             // 9.6 kb/s
        {0x41,    0x00,   (UCBRF_2+UCOS16)},          // 19.2 kb/s
        {0x20,    0x00,   (UCBRF_9+UCOS16)},          // 38.4 kb/s
        {0x15,    0x00,   (UCBRF_11+UCOS16)},         // 57.6 kb/s
        {0x0A,    0x00,   (UCBRF_14+UCOS16)},         // 115.2 kbit/s
        {0x05,    0x00,   (UCBRF_7+UCOS16)},          // 230.4 kbit/s
        {0x02,    0x00,   (UCBRS_6+UCBRF_10+UCOS16)}, // 460.8 kbit/s
        {0x15,    0x00,   (UCBRS_6)}                  // 921.6 kbit/s
};

// Baudrate indexes
enum baud{BAUD_ERROR, BAUD_1200, BAUD_2400, BAUD_4800, BAUD_9600, BAUD_19200,
    BAUD_38400, BAUD_57600, BAUD_115200, BAUD_230400, BAUD_460800, BAUD_921600};

uint8_t InitUart(uint32_t lBaudrate)
{
    uint8_t baudIndex;

    //Simple port mapping
    __disable_interrupt();                  // Disable Interrupts before altering Port Mapping registers
    PMAPKEYID = PMAPKEY;
    P4MAP0 = PM_NONE;
    P4MAP3 = PM_NONE;
    P4MAP4 = PM_UCA0TXD;
    P4MAP5 = PM_UCA0RXD;
    PMAPKEYID = 0;
    __enable_interrupt();

    P4SEL |= BIT0 + BIT3 + BIT4 + BIT5;

    // configure USCI_A0 UART
    UCA0CTL1 |= UCSWRST;                    // **Put state machine in reset**
    UCA0CTL1 |= UCSSEL__SMCLK;              // SMCLK
    UCA0CTL0 = UCPEN+UCPAR;

    switch(lBaudrate)
    {
        #if (RX_BUFFER_SIZE > (1200*SYS_DELAY/1000/8))
            case 1200: baudIndex = BAUD_1200; break;
        #endif
        #if (RX_BUFFER_SIZE > (2400*SYS_DELAY/1000/8))
            case 2400: baudIndex = BAUD_2400; break;
        #endif
        #if(RX_BUFFER_SIZE > (4800*SYS_DELAY/1000/8))
            case 4800: baudIndex = BAUD_4800; break;
        #endif
            case 4801:                      // Invoke Rocket's BSL
                __disable_interrupt();
                USB_disable();
                __delay_cycles(500000);
                ((void(*)(void))0x1000)();  // Call #0x1000, enter BSL
                break;

            case 4802:
                BSL_reset_sequence();
                baudIndex = BAUD_9600;
                break;
        #if(RX_BUFFER_SIZE > (9600*SYS_DELAY/1000/8))
            case 9600: baudIndex = BAUD_9600; break;
        #endif
            case 9601:
                BSL_invoke_sequence();
                baudIndex = BAUD_9600;
                break;
        #if(RX_BUFFER_SIZE > (19200*SYS_DELAY/1000/8))
            case 19200: baudIndex = BAUD_19200; break;
        #endif
        #if(RX_BUFFER_SIZE > (38400*SYS_DELAY/1000/8))
            case 38400: baudIndex = BAUD_38400; break;
        #endif
        #if(RX_BUFFER_SIZE > (57600*SYS_DELAY/1000/8))
            case 57600: baudIndex = BAUD_57600; break;
        #endif
        #if(RX_BUFFER_SIZE > (115200*SYS_DELAY/1000/8))
            case 115200: baudIndex = BAUD_115200; break;
        #endif
        #if(RX_BUFFER_SIZE > (230400*SYS_DELAY/1000/8))
            case 230400: baudIndex = BAUD_230400; break;
        #endif
        #if(RX_BUFFER_SIZE > (460800*SYS_DELAY/1000/8))
            case 460800: baudIndex = BAUD_460800; break;
        #endif
        #if(RX_BUFFER_SIZE > (921600*SYS_DELAY/1000/8))
            case 921600: baudIndex = BAUD_921600; break;
        #endif
        default: baudIndex = BAUD_ERROR; break;
    }

    if(baudIndex != BAUD_ERROR)
    {
        UCA0BR0 = BaudrateList[baudIndex].ucaBR0;
        UCA0BR1 = BaudrateList[baudIndex].ucaBR1;
        UCA0MCTL = BaudrateList[baudIndex].ucaMCTL;
    }

    UCA0CTL1 &= ~UCSWRST;                   // Initialize USCI state machine

    UCA0IE |= UCRXIE;

    return baudIndex;
}

void uartControl()
{
    /* Send data from USB buffer to UART */
    if(USBCDC_bytesInUSBBuffer(CDC0_INTFNUM) > 0)
    {
        if(UCA0IFG & UCTXIFG)
        {
            USBCDC_receiveData((uint8_t *)&UCA0TXBUF, 1, CDC0_INTFNUM);
            LED2_TOGGLE
        }
    }

    if(UARTtoCDC_TXPtr != UARTtoCDC_RXPtr)
    {
        while(cdcSendDataWaitTilDone((uint8_t*)&UARTtoCDC_CircularBuffer[UARTtoCDC_TXPtr], 1, CDC0_INTFNUM, 100000) != 0);
        UARTtoCDC_TXPtr++;
        if (UARTtoCDC_TXPtr == BUFFER_SIZE)
        {
            UARTtoCDC_TXPtr = 0;
        }
    }
}

#pragma vector=USCI_A0_VECTOR
__interrupt void USCI_A0_ISR(void)
{
    switch(__even_in_range(UCA0IV,4))
    {
        case 0:                               // Vector 0 - no interrupt
            break;
        case 2:                               // Vector 2 - RXIFG
            // Store received byte in TX buffer
            UARTtoCDC_CircularBuffer[UARTtoCDC_RXPtr++] = UCA0RXBUF;
            if (UARTtoCDC_RXPtr == BUFFER_SIZE)
            {
                UARTtoCDC_RXPtr = 0;
            }
            break;
        case 4:	                              // Vector 4 - TXIFG
            __no_operation();                 // used for debugging
            break;
        default: break;
    }
}


/*** I2C *********************************************************************/
#define UART_HEADER         0x80
#define UART_CMD_HEADER     0xA0
#define UART_HEADER_LENGTH  1     // length in byte
#define UART_LENGTH_LENGTH  2     // length in byte
#define UART_CRC_LENGTH     2     // length in byte
#define UART_CMD_LENGTH     1     // length in byte

#define TIMEOUT_RECEPTION  1000000

#define BSL_RESPONSE_ACK	0x00

// define STATES
#define SWAIT	0x00
#define SSTART	0x01
#define SLENGTH	0x02
#define SDATA	0x03
#define SSEND	0x04
#define SCMD    0x05

unsigned char ret;
unsigned int bytes_sent, bytes_received;
int16_t sendDataLength = 0;
unsigned int BSLCommandLength = 0;
int16_t requireSlaveAnswer = 1;
unsigned char dataBuffer[300];
volatile unsigned char bDataSendCompleted_event[CDC_NUM_INTERFACES] = {FALSE};
volatile unsigned char bDataReceiveCompleted_event[CDC_NUM_INTERFACES] = {FALSE};  // data receive completed event
unsigned char *PTxData;                     // Pointer to TX data
int TXByteCtr;
unsigned char *PRxData;                     // Pointer to RX data
int RXByteCtr;
unsigned char RxBuffer[256];

unsigned char ACK = 0;
unsigned int rxlength = 0;

unsigned int length = 0;
unsigned char state = SWAIT;

unsigned char CoreCommand[256];
unsigned char i = 0;

extern volatile unsigned char bDataReceiveCompleted_event[];  // data receive completed event

enum
{
    I2C_IDLE,
    I2C_RECEIVING,
    I2C_RECEIVE_COMPLETE,
    I2C_TRANSMITTING,
    I2C_TRANSMIT_COMPLETE,
    I2C_ERROR
} I2C_Status_e;


//!  Initialization of the USCI Module for I2C
int8_t InitI2C(unsigned char eeprom_i2c_address, uint32_t bitrate)
{
    int8_t ret = 1;

    //Simple port mapping
    __disable_interrupt();                // Disable Interrupts before altering Port Mapping registers
    PMAPKEYID = PMAPKEY;
    P4MAP0 = PM_UCB0SCL;
    P4MAP3 = PM_NONE;
    P4MAP4 = PM_NONE;
    P4MAP5 = PM_UCB0SDA;
    PMAPKEYID = 0;
    __enable_interrupt();

    P4SEL |= BIT0 + BIT3 + BIT4 + BIT5;

    UCB0CTL1 = UCSWRST;                         // Enable SW reset

    UCB0CTL0 = UCMST + UCMODE_3 + UCSYNC;       // I2C Master, synchronous mode
    UCB0CTL1 = UCSSEL_2 + UCSWRST;              // Use SMCLK, keep SW reset

    switch(bitrate)
    {
        case 100001:
            P1OUT ^= (BIT0|BIT1);
            BSL_invoke_sequence();
            P1OUT |= (BIT0);
            UCB0BR0 = SCL_CLOCK_DIV(100000);                    // set prescaler
            break;
        case 100000:
            P1OUT |= (BIT0);
            UCB0BR0 = SCL_CLOCK_DIV(100000);                    // set prescaler
            break;
        case 400001:
            P1OUT ^= (BIT0|BIT1);
            BSL_invoke_sequence();
            P1OUT |= (BIT1);
            UCB0BR0 = SCL_CLOCK_DIV(400000);                    // set prescaler
            break;
        case 400000:
            P1OUT |= (BIT1);
            UCB0BR0 = SCL_CLOCK_DIV(400000);                    // set prescaler
            break;
        default:
            ret = 1;
    }

    UCB0BR1 = 0;
    UCB0I2CSA = eeprom_i2c_address;             // Set slave address

    I2C_PORT_SEL |= SDA_PIN + SCL_PIN;          // select module function for the used I2C pins

    UCB0CTL1 &= ~UCSWRST;                       // Clear SW reset, resume operation

    PRxData = RxBuffer;                         // Incase no receive buffer is assigned

    if(UCB0STAT & UCBBUSY)                      // test if bus to be free
    {                                           // otherwise a manual Clock on is generated
        I2C_PORT_SEL &= ~SCL_PIN;               // Select Port function for SCL
        I2C_PORT_OUT &= ~SCL_PIN;               //
        I2C_PORT_DIR |= SCL_PIN;                // drive SCL low
        I2C_PORT_SEL |= SDA_PIN + SCL_PIN;      // select module function for the used I2C pins
    };

    I2C_Status_e = I2C_IDLE;

    return ret;
}

int16_t i2cSendMessage(unsigned char* I2cMessage, int messageLength)
{
    UCB0IE &= ~UCRXIE;                            // disable RX ready interrupt
    UCB0IFG &= ~(UCTXIFG + UCSTPIFG + UCNACKIFG); // clear TX ready and stop interrupt flag
    UCB0IE |= UCTXIE | UCNACKIE;                  // enable TX ready interrupt

    PTxData = (unsigned char *)I2cMessage;        // TX array start address

    TXByteCtr = messageLength ;                   // Load TX byte counter

    I2C_Status_e = I2C_TRANSMITTING;
    UCB0CTL1 |= UCTR + UCTXSTT;                   // I2C TX, start condition
    // while (UCB0CTL1 & UCTXSTT);                 // Start condition sent?
    // while (UCB0STAT & UCBBUSY);                 // wait for bus to be free  !!!!!
    // UCB0IE &= ~UCSTPIE;                         // disable STOP interrupt

    while(I2C_Status_e == I2C_TRANSMITTING);

    if(I2C_Status_e == I2C_TRANSMIT_COMPLETE)
    {
        // check for commands that doesn't require an ack
        PTxData = (unsigned char *)I2cMessage;        // TX array start address

        if((*(PTxData + UART_HEADER_LENGTH + UART_LENGTH_LENGTH) == 0x1B))
            return 0;
        else
            return 1;
    }
    else if(I2C_Status_e == I2C_ERROR)
    {
        return -1;
    }

    return -1;

}


// returns number of received bytes
int16_t i2cReceiveMessage(unsigned char* I2cMessage)
{
    uint32_t timeout;

    UCB0CTL1 &= ~UCTR;
    UCB0IE |= UCRXIE;                         // Enable RX interrupt

    PRxData = (unsigned char *)I2cMessage;    // Start of RX buffer
    RXByteCtr = 1;                            // Load RX byte counter

    ACK = 0;
    I2C_Status_e = I2C_RECEIVING;
    UCB0CTL1 |= UCTXSTT;
    //__bis_SR_register(LPM0_bits + GIE);       // Enter LPM0, enable interrupts
                                              // Remain in LPM0 until all data
                                              // is RX'd
    timeout = TIMEOUT_RECEPTION;
    while((I2C_Status_e == I2C_RECEIVING) && (timeout-- != 0x00));

    if((timeout == 0) || (I2C_Status_e == I2C_ERROR))
    {
        i2cStopSending();
        timeout = TIMEOUT_RECEPTION;
        while((UCB0CTL1 & UCTXSTP) && (timeout-- != 0));
    }

    if(I2C_Status_e == I2C_RECEIVE_COMPLETE)
    {
        return (rxlength > 0) ? 1 + I2C_HEADER_LENGTH + I2C_LENGTH_LENGTH + rxlength + I2C_CRC_LENGTH : 1 + I2C_HEADER_LENGTH + I2C_LENGTH_LENGTH;
    }
    else
    {
        return -1;
    }
}


void i2cStopSending(void)
{
    UCB0CTL1 |= UCTXSTP;
}


void i2cControl()
{
    ret = USBCDC_intfStatus(CDC0_INTFNUM, &bytes_sent,  &bytes_received);
    if (ret & kUSBCDC_dataWaiting)
    {
        BSLCommandLength = UART_FSM(dataBuffer);
    }
    if (BSLCommandLength > 0)
    {
        // Special command to just get data
        if (BSLCommandLength == 0x01)
        {
            // If response is OK, request response from Slave
            sendDataLength = i2cReceiveMessage(dataBuffer);
            if ((requireSlaveAnswer < 0) || (sendDataLength < 0))
            {
                // If there was an error, report to PC
                sendDataLength = 1;
                dataBuffer[0] = 0x55;   // Temporary error code
            }
        }
        else
        {
            requireSlaveAnswer = i2cSendMessage(dataBuffer, BSLCommandLength);
            i2cStopSending();
            __delay_cycles(10000);

            if (requireSlaveAnswer == 0x00)
            {
                sendDataLength = 0; // Don't send response to PC
            }
            else if(requireSlaveAnswer > 0)
            {
                // If response is OK, request response from Slave
                sendDataLength = i2cReceiveMessage(dataBuffer);
            }

            if ((requireSlaveAnswer < 0) || (sendDataLength < 0))
            {
                // If there was an error, report to PC
                sendDataLength = 1;
                dataBuffer[0] = 0x55;   // Temporary error code
            }
            BSLCommandLength = 0;
        }
    }

    if (sendDataLength > 0)
    {
        //send data back to PC
        bDataSendCompleted_event[0] = FALSE;
        ret = USBCDC_sendData((unsigned char*)&dataBuffer, sendDataLength, CDC0_INTFNUM);
        while (bDataSendCompleted_event == FALSE){};
        sendDataLength = 0;
    }
}


unsigned int UART_FSM(unsigned char* dataBuffer)
{
    unsigned int retvalue = 0;

    switch(state)
    {
        case(SWAIT):	// wait for UART_HEADER to start the data sequence
        USBCDC_receiveData(dataBuffer, UART_HEADER_LENGTH, CDC0_INTFNUM);
        if(dataBuffer[0] == UART_HEADER)
        {
            length = 0;
            state = SSTART;
        }
        else if (dataBuffer[0] == UART_CMD_HEADER)
        {
            state = SCMD;
        }

        retvalue = 0;
        break;
        case(SSTART):	// read in length bytes
            bDataReceiveCompleted_event[0] = FALSE;
            USBCDC_receiveData(dataBuffer + UART_HEADER_LENGTH, UART_LENGTH_LENGTH, CDC0_INTFNUM);
            while (bDataReceiveCompleted_event[0] == FALSE){};		// wait until data received
            length = ((unsigned char) dataBuffer[1 + UART_HEADER_LENGTH] << 8) | dataBuffer[0 + UART_HEADER_LENGTH];
            state = SDATA;
            retvalue = 0;
            break;
        case(SDATA):	// read in all data (length bytes) plus CRC
            bDataReceiveCompleted_event[0] = FALSE;
            USBCDC_receiveData(dataBuffer + UART_HEADER_LENGTH + UART_LENGTH_LENGTH, length + UART_CRC_LENGTH, CDC0_INTFNUM);
            while (bDataReceiveCompleted_event[0] == FALSE){};

            USBCDC_rejectData(CDC0_INTFNUM);		// discard leftover bytes

            state = SWAIT;
            retvalue = UART_HEADER_LENGTH + UART_LENGTH_LENGTH + length + UART_CRC_LENGTH;
            break;

        case SCMD:
            USBCDC_receiveData(dataBuffer, UART_CMD_LENGTH, CDC0_INTFNUM);
            retvalue = dataBuffer[0];
            state = SWAIT;
            break;

        default:
            state = SWAIT;
            retvalue = 0;
            break;
    }
    return retvalue;
}


// ISR for I2C
#pragma vector = USCI_B0_VECTOR
__interrupt void USCI_B0_ISR(void)
{
    switch(__even_in_range(UCB0IV,USCI_I2C_UCTXIFG))
    {
        case USCI_NONE:          break; // Vector 0:  No interrupts
        case USCI_I2C_UCALIFG:   break; // Vector 2:  UCALIFG
        case USCI_I2C_UCNACKIFG:
            I2C_Status_e = I2C_ERROR;
            break;                      // Vector 4:  NACKIFG
        case USCI_I2C_UCSTTIFG:  break; // Vector 6:  STTIFG
        case USCI_I2C_UCSTPIFG:  break; // Vector 8:  STPIFG
        case USCI_I2C_UCRXIFG:          // Vector 10: RXIFG

            if (ACK == 0)
            {
                *PRxData++ = UCB0RXBUF;               // Move RX data to address PRxData
                if(*(PRxData-1) != BSL_RESPONSE_ACK)
                {
                    UCB0CTL1 |= UCTXSTP;              // Generate I2C stop condition
                }
                else
                {
                    ACK = 1;
                    RXByteCtr = I2C_HEADER_LENGTH + I2C_LENGTH_LENGTH;  // receives the next three bytes
                    rxlength = 0;
                }
            }
            else
            {
                RXByteCtr--;                            // Decrement RX byte counter
                if(RXByteCtr)
                {
                    *PRxData++ = UCB0RXBUF;               // Move RX data to address PRxData
                    if((RXByteCtr == 1) && (rxlength > 0)) // Only one byte left?
                    {
                        UCB0CTL1 |= UCTXSTP;                // Generate I2C stop condition
                    }
                }
                else if(rxlength == 0)
                {
                    *PRxData++ = UCB0RXBUF;               // Move RX data to address PRxData
                    rxlength = ((unsigned int) *(PRxData-1) << 8) | *(PRxData-2);
                    if(rxlength == 0) UCB0CTL1 |= UCTXSTP;	// stop receiving if no bytes required
                    {
                        RXByteCtr = rxlength + I2C_CRC_LENGTH;
                    }
                }
                else
                {
                    *PRxData = UCB0RXBUF;                 // Move final RX data to PRxData
                    I2C_Status_e = I2C_RECEIVE_COMPLETE;
                    //__bic_SR_register_on_exit(LPM0_bits); // Exit active CPU
                }
            }
            break;
        case USCI_I2C_UCTXIFG:   				  // Vector 12: TXIFG
            if(TXByteCtr)                          // Check TX byte counter
            {
                UCB0TXBUF = *PTxData++;               // Load TX buffer
                TXByteCtr--;                          // Decrement TX byte counter
            }
            else
            {
                //UCB0CTL1 |= UCTXSTP;                // Generate I2C stop condition
                I2C_Status_e = I2C_TRANSMIT_COMPLETE;
                //__bic_SR_register_on_exit(LPM0_bits); // Exit active CPU
            }
            break;
        default:
            break;
    }
}


/*** SPI *********************************************************************/
uint8_t spiInit(uint32_t lBaudrate)
{
    //Simple port mapping
    __disable_interrupt();                  // Disable Interrupts before altering Port Mapping registers
    PMAPKEYID = PMAPKEY;
    P4MAP0 = PM_UCB1CLK;
    P4MAP3 = PM_NONE;  // STE is driven by I/O pin (see below)
    P4MAP4 = PM_UCB1SIMO;
    P4MAP5 = PM_UCB1SOMI;
    PMAPKEYID = 0;
    __enable_interrupt();

    P4SEL = BIT0 + BIT4 + BIT5;

    // Use P4.3 as (active low) STE signal
    P4DIR |= BIT3;
    P4OUT |= BIT3;  // Disable SPI slave

    // configure USCI_B1 SPI
    UCB1CTL1 = UCSWRST;                     // **Put state machine in reset**
    UCB1CTL1 |= UCSSEL__ACLK;
    // CKPH=0, idle high (CKPL=1), MSB first, master, 3-pin SPI, synchronous mode
    UCB1CTL0 |= UCCKPL + UCMSB + UCMST + UCMODE_0 + UCSYNC;

    switch(lBaudrate)
    {
    case 125000:
        UCB1BRW = 32;
        break;
    case 125001:
        BSL_invoke_sequence();
        UCB1BRW = 32;
        break;
    case 250000:
        UCB1BRW = 16;
        break;
    case 250001:
        BSL_invoke_sequence();
        UCB1BRW = 16;
        break;
    case 500000:
        UCB1BRW = 8;
        break;
    case 500001:
        BSL_invoke_sequence();
        UCB1BRW = 8;
        break;
    case 1000000:
        UCB1BRW = 4;
        break;
    case 1000001:
        BSL_invoke_sequence();
        UCB1BRW = 4;
        break;
    default:
        return 1;
        break;
    }

    UCB1CTL1 &= ~UCSWRST;                   // Initialize USCI state machine

    return 0;
}

void spiControl()
{
    static uint16_t TXresponseLength = 0;
    static uint16_t TXresponseIndex = 0;
    static uint16_t RXresponseLength = 0;
    static uint16_t RXresponseIndex = 0;
    static enum spiStates spiState = idle;

    switch(spiState)
    {
    case idle:
        {
            P4OUT |= BIT3;  // Disable SPI slave
            /* Send data from USB buffer to SPI */
            if(USBCDC_bytesInUSBBuffer(CDC0_INTFNUM) > 0)
            {
                P4OUT &= ~BIT3;  // Disable SPI slave
                uint8_t txChar = 0;
                UCB1IFG &= ~UCRXIFG;  // Clear RX IRQ flag
                USBCDC_receiveData(&txChar, 1, CDC0_INTFNUM);
                while(!(UCB1IFG & UCTXIFG));
                UCB1TXBUF = txChar;
                LED2_TOGGLE
                if(txChar == 0x80)  // Check for BSL header from host
                {
                    UCB1IFG &= ~UCRXIFG;  // Clear RX IRQ flag
                    spiState = TXlengthLow;
                }
                else
                {
                    while(!(UCB1IFG & UCRXIFG));
                    // Store received byte in TX buffer
                    *txBuffer_write_ptr = UCB1RXBUF;
                    txBuffer_write_ptr++;
                    if(txBuffer_write_ptr - txBuffer == TX_BUFFER_SIZE)
                    {
                        txBuffer_write_ptr = txBuffer;
                    }
                }
            }
            break;
        }
    case TXlengthLow:
        {
            if(USBCDC_bytesInUSBBuffer(CDC0_INTFNUM) > 0)
            {
                uint8_t txChar = 0;
                UCB1IFG &= ~UCRXIFG;  // Clear RX IRQ flag
                USBCDC_receiveData(&txChar, 1, CDC0_INTFNUM);
                while(!(UCB1IFG & UCTXIFG));
                UCB1TXBUF = txChar;
                LED2_TOGGLE
                TXresponseLength = (uint16_t)txChar;
                spiState = TXlengthHigh;
            }
            break;
        }
    case TXlengthHigh:
        {
            if(USBCDC_bytesInUSBBuffer(CDC0_INTFNUM) > 0)
            {
                uint8_t txChar = 0;
                UCB1IFG &= ~UCRXIFG;  // Clear RX IRQ flag
                USBCDC_receiveData(&txChar, 1, CDC0_INTFNUM);
                while(!(UCB1IFG & UCTXIFG));
                UCB1TXBUF = txChar;
                LED2_TOGGLE
                TXresponseLength |= (uint16_t)txChar << 8;
                spiState = TXreceive;
            }
            break;
        }
    case TXreceive:
        {
            while(TXresponseIndex < TXresponseLength + SPI_CRC_LENGTH)  // Wait for BSL command to be send from host completely
            {
                /* Send data from USB buffer to SPI */
                if(USBCDC_bytesInUSBBuffer(CDC0_INTFNUM) > 0)
                {
                    UCB1IFG &= ~UCRXIFG;  // Clear RX IRQ flag
                    while(!(UCB1IFG & UCTXIFG));
                    USBCDC_receiveData((uint8_t *)&UCB1TXBUF, 1, CDC0_INTFNUM);
                    LED2_TOGGLE
                    TXresponseIndex++;
                }
            }
            if(TXresponseIndex == TXresponseLength + SPI_CRC_LENGTH)
            {
                __delay_cycles(500000);  // Wait for target
                UCB1IFG &= ~UCRXIFG;  // Clear RX IRQ flag
                // Shift SPI to receive BSL response
                while(!(UCB1IFG & UCTXIFG));
                UCB1TXBUF = 0xFF;
                while(UCB1STAT & UCBUSY);  // Wait for completed send operation
                // Send response to host
                while(!(UCB1IFG & UCRXIFG));
                uint8_t rxChar = UCB1RXBUF;
                // Store received byte in TX buffer
                *txBuffer_write_ptr = rxChar;
                txBuffer_write_ptr++;
                if(txBuffer_write_ptr - txBuffer == TX_BUFFER_SIZE)
                {
                    txBuffer_write_ptr = txBuffer;
                }
                if(rxChar != 0x00)  // BSL NACK
                {
                    spiState = idle;
                }
                else  // BSL ACK Retrieve and send BSL core response
                {
                    spiState = RXheader;
                }
                TXresponseLength = 0;
                TXresponseIndex = 0;
            }
            break;
        }
    case RXheader:
        {
            // Shift SPI to receive BSL response
            while(!(UCB1IFG & UCTXIFG));
            UCB1TXBUF = 0xFF;
            while(UCB1STAT & UCBUSY);  // Wait for completed send operation
            // Send response to host
            while(!(UCB1IFG & UCRXIFG));
            uint8_t rxChar = UCB1RXBUF;
            for(uint8_t retryIndex = 50; retryIndex > 0; retryIndex--)
            {
                if(rxChar != 0x80)
                {
                    __delay_cycles(250000);  // Wait ~10ms for BSL response
                    // Shift SPI to receive BSL response
                    while(!(UCB1IFG & UCTXIFG));
                    UCB1TXBUF = 0xFF;
                    while(UCB1STAT & UCBUSY);  // Wait for completed send operation
                    // Send response to host
                    while(!(UCB1IFG & UCRXIFG));
                    rxChar = UCB1RXBUF;
                } else {
                    break;
                }
            }
            // Store received byte in TX buffer
            *txBuffer_write_ptr = rxChar;
            txBuffer_write_ptr++;
            if(txBuffer_write_ptr - txBuffer == TX_BUFFER_SIZE)
            {
                txBuffer_write_ptr = txBuffer;
            }
            spiState = RXlengthLow;
            break;
        }
    case RXlengthLow:
        {
            // Shift SPI to receive BSL response
            while(!(UCB1IFG & UCTXIFG));
            UCB1TXBUF = 0xFF;
            while(UCB1STAT & UCBUSY);  // Wait for completed send operation
            // Send response to host
            while(!(UCB1IFG & UCRXIFG));
            uint8_t rxChar = UCB1RXBUF;
            RXresponseLength = (uint16_t)rxChar;
            // Store received byte in TX buffer
            *txBuffer_write_ptr = rxChar;
            txBuffer_write_ptr++;
            if(txBuffer_write_ptr - txBuffer == TX_BUFFER_SIZE)
            {
                txBuffer_write_ptr = txBuffer;
            }
            spiState = RXlengthHigh;
            break;
        }
    case RXlengthHigh:
        {
            // Shift SPI to receive BSL response
            while(!(UCB1IFG & UCTXIFG));
            UCB1TXBUF = 0xFF;
            while(UCB1STAT & UCBUSY);  // Wait for completed send operation
            // Send response to host
            while(!(UCB1IFG & UCRXIFG));
            uint8_t rxChar = UCB1RXBUF;
            RXresponseLength |= (uint16_t)rxChar << 8;
            // Store received byte in TX buffer
            *txBuffer_write_ptr = rxChar;
            txBuffer_write_ptr++;
            if(txBuffer_write_ptr - txBuffer == TX_BUFFER_SIZE)
            {
                txBuffer_write_ptr = txBuffer;
            }
            spiState = RXreceive;
            break;
        }
    case RXreceive:
        {
            if(RXresponseIndex < RXresponseLength + SPI_CRC_LENGTH)  // Wait for BSL response to be received completely
            {
                // Shift SPI to receive BSL response
                while(!(UCB1IFG & UCTXIFG));
                UCB1TXBUF = 0xFF;
                while(UCB1STAT & UCBUSY);  // Wait for completed send operation
                // Send response to host
                while(!(UCB1IFG & UCRXIFG));
                // Store received byte in TX buffer
                *txBuffer_write_ptr = UCB1RXBUF;
                txBuffer_write_ptr++;
                if(txBuffer_write_ptr - txBuffer == TX_BUFFER_SIZE)
                {
                    txBuffer_write_ptr = txBuffer;
                }
                RXresponseIndex++;
            }
            if(RXresponseIndex == RXresponseLength + SPI_CRC_LENGTH)
            {
                RXresponseLength = 0;
                RXresponseIndex = 0;
                spiState = idle;
            }
            break;
        }
    default:
        break;
    }

    /* Send data from USB buffer to host */
    if(txBuffer_read_ptr - txBuffer == TX_BUFFER_SIZE)
        {
            txBuffer_read_ptr = txBuffer;
        }
    if(txBuffer_read_ptr != txBuffer_write_ptr)
    {
        /* New data in txBuffer, send it */
        uint16_t bytes_to_send = 0;
        // In case the write ptr < the read ptr sent till end of buffer...
        if(txBuffer_write_ptr < txBuffer_read_ptr)
        {
            bytes_to_send = TX_BUFFER_SIZE - (txBuffer_read_ptr - txBuffer);
            if(kUSBCDC_sendStarted == USBCDC_sendData(txBuffer_read_ptr, bytes_to_send, CDC0_INTFNUM))
            {
                txBuffer_read_ptr += bytes_to_send;
            }
        }
        // ... else sent till catching up with the write pts.
        else
        {
            bytes_to_send = txBuffer_write_ptr - txBuffer_read_ptr;
            if(kUSBCDC_sendStarted == USBCDC_sendData(txBuffer_read_ptr, bytes_to_send, CDC0_INTFNUM))
            {
                txBuffer_read_ptr += bytes_to_send;
            }
        }
    }
}
